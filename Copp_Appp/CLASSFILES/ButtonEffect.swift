//
//  ButtonEffect.swift
//  NHS
//
//  Created by RMV on 02/02/18.
//  Copyright © 2018 NHS. All rights reserved.
//

import UIKit

@IBDesignable
class ButtonEffect: UIButton {
    

//    @IBInspectable var CornerRedius:Bool = false{
//        didSet{
//            if CornerRedius {
//                layer.cornerRadius = 10
//            }
//        }
//    }
//    
//    override func prepareForInterfaceBuilder() {
//        if CornerRedius {
//            layer.cornerRadius = 10
//        }
//    }
    
    @IBInspectable var borderColor: UIColor = UIColor.black {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 1.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    

    override public func layoutSubviews() {
        super.layoutSubviews()
       // layer.cornerRadius = 0.5 * bounds.size.width
        layer.cornerRadius = 5.0
        clipsToBounds = true
    }
    

 
}
