//
//  DroplistDetailVC.swift
//  Copp_Appp
//
//  Created by siya enterprise on 12/06/18.
//  Copyright © 2018 siya enterprise. All rights reserved.
//

import UIKit

class DroplistDetailVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBarController?.tabBar.isHidden = true

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

    @IBAction func backtohomescreen(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func BuyMembershipClicked(_ sender: UIButton) {
        
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DroplistMembershipVC")
        
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
}
