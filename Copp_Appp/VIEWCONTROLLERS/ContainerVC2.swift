//
//  ContainerVC2.swift
//  Copp_Appp
//
//  Created by siya enterprise on 11/06/18.
//  Copyright © 2018 siya enterprise. All rights reserved.
//

import UIKit

class ContainerVC2: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collect: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        collect.delegate = self
        collect.dataSource = self
        
         tabBarController?.tabBar.isHidden = false

       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 3
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ContainerCell2
        
        //  cell.lblproductname.text = "chair"
        
        cell.lblproductname.text = "ASSC HMU White/Yellow"
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //return CGSize(width: collect.frame.width / 2 - 2, height: collect.frame.width / 2 - 2)
        
        return CGSize(width:(collect.frame.size.width / 2) - 15, height: 200)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(10, 10, 10, 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 10.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 10.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DroplistDetailVC")
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
}
