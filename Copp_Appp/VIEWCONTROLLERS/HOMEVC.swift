//
//  HOMEVC.swift
//  Copp_Appp
//
//  Created by siya enterprise on 11/06/18.
//  Copyright © 2018 siya enterprise. All rights reserved.
//

import UIKit
import Alamofire

class HOMEVC: UIViewController {
    
   
    @IBOutlet weak var btn_droplist: UIButton!
    @IBOutlet weak var btn_buynow: UIButton!
    @IBOutlet weak var btn_designer: UIButton!
    
    @IBOutlet weak var containerview3: UIView!
    @IBOutlet weak var containerview2: UIView!
    @IBOutlet weak var containerView1: UIView!
    @IBOutlet weak var viewsegment: UIView!
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        containerview2.isHidden = false
        containerView1.isHidden = true
        containerview3.isHidden = true
        
        btn_droplist.backgroundColor = UIColor.black
        btn_droplist.setTitleColor(UIColor.white, for: .normal)
        
        btn_designer.setTitleColor(UIColor.black, for: .normal)
        btn_buynow.setTitleColor(UIColor.black, for: .normal)
      
        
         tabBarController?.tabBar.isHidden = false
        
        let attr = NSDictionary(object: UIFont(name: "HelveticaNeue-Bold", size: 16.0)!, forKey: NSAttributedStringKey.font as NSCopying)
        
        UISegmentedControl.appearance().setTitleTextAttributes(attr as [NSObject : AnyObject] , for: .normal)

       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // tabBarController?.tabBar.isHidden = false
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func droplistclicked(_ sender: UIButton)
    {
        
         btn_droplist.backgroundColor = UIColor.black
         btn_buynow.backgroundColor = UIColor.white
         btn_designer.backgroundColor = UIColor.white
        
        containerview2.isHidden = false
        containerView1.isHidden = true
        containerview3.isHidden = true
        
         btn_droplist.setTitleColor(UIColor.white, for: .normal)
         btn_buynow.setTitleColor(UIColor.black, for: .normal)
         btn_designer.setTitleColor(UIColor.black, for: .normal)
        
        
    }
    @IBAction func buynowclicked(_ sender: UIButton) {
        
        btn_buynow.backgroundColor = UIColor.black
        btn_droplist.backgroundColor = UIColor.white
        btn_designer.backgroundColor = UIColor.white
        
        containerview2.isHidden = true
        containerView1.isHidden = false
        containerview3.isHidden = true
        
     
        btn_buynow.setTitleColor(UIColor.white, for: .normal)
         btn_designer.setTitleColor(UIColor.black, for: .normal)
        btn_droplist.setTitleColor(UIColor.black, for: .normal)
        
        
    }
    
    @IBAction func designerclicked(_ sender: UIButton) {
        
        
        btn_designer.backgroundColor = UIColor.black
        btn_buynow.backgroundColor = UIColor.white
        btn_droplist.backgroundColor = UIColor.white
        containerview2.isHidden = true
        containerView1.isHidden = true
        containerview3.isHidden = false
        
        btn_designer.setTitleColor(UIColor.white, for: .normal)
        btn_buynow.setTitleColor(UIColor.black, for: .normal)
         btn_droplist.setTitleColor(UIColor.black, for: .normal)
        
        
    }

}

