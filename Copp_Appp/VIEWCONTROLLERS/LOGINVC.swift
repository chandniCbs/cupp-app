//
//  LOGINVC.swift
//  Copp_Appp
//
//  Created by siya enterprise on 11/06/18.
//  Copyright © 2018 siya enterprise. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import FBSDKShareKit
import GoogleSignIn
import Alamofire
//import FacebookLogin

class LOGINVC: UIViewController, GIDSignInDelegate, GIDSignInUIDelegate, UITextFieldDelegate {
    
    let UserNameValidation = "Please enter Username"
    let PasswordValidation = "Please enter Password"
    
    @IBOutlet weak var Alertview: UIView!
    @IBOutlet weak var AlertForgotpasswordview: UIView!
    var dict : [String : AnyObject]!

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txt_password: UITextField!
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var passView: UIView!
    @IBOutlet weak var EmailView: UIView!
     @IBOutlet weak var view_txt_forgotpass_textfield_View1: UIView!
    
    
    @IBOutlet weak var lbl_titleAlertview1: UILabel!
    @IBOutlet weak var lbl_lbl_titleAlertview2: UILabel!
   
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        txt_email.delegate = self
        txt_password.delegate = self
         Alertview.isHidden = true
         AlertForgotpasswordview.isHidden = true
        
        EmailView.layer.borderColor = UIColor(red:187/255, green:225/255, blue:250/255, alpha: 1).cgColor
        
        EmailView.layer.cornerRadius = 5
        EmailView.layer.borderWidth = 2
        
        passView.layer.borderColor = UIColor(red:187/255, green:225/255, blue:250/255, alpha: 1).cgColor
        
        passView.layer.cornerRadius = 5
        passView.layer.borderWidth = 2
      
    }
    @IBAction func OkbtnClicked(_ sender: UIButton) {
        
        Alertview.isHidden = true
    }
    @IBAction func RequestbtnClicked(_ sender: UIButton) {
        
         AlertForgotpasswordview.isHidden = true
    }
     @IBAction func Forgotpasswordclicked(_ sender: UIButton) {
        
         AlertForgotpasswordview.isHidden = false
        
        view_txt_forgotpass_textfield_View1.layer.borderColor = UIColor(red:21/255, green:181/255, blue:188/255, alpha: 1).cgColor
        
        view_txt_forgotpass_textfield_View1.layer.cornerRadius = 5
        view_txt_forgotpass_textfield_View1.layer.borderWidth = 2
        
        AlertForgotpasswordview.layer.borderColor = UIColor(red:21/255, green:181/255, blue:188/255, alpha: 1).cgColor
        
        AlertForgotpasswordview.layer.cornerRadius = 20
        AlertForgotpasswordview.layer.borderWidth = 3
    }
    @IBAction func LoginBtn(_ sender: UIButton) {
        self.view.endEditing(true)
        if Validate(){
            
            let TabBarController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarCon") as! UITabBarController
            
            self.navigationController?.pushViewController(TabBarController, animated: true)
            
        }
    }
    @IBAction func BackbtnClicked(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func LoginwithfacebookClicked(_ sender: UIButton) {
        
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) -> Void in
            if (error == nil){
                
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                
                if (result?.isCancelled)!{
                    return
                }
                if(fbloginresult.grantedPermissions.contains("email"))
                {
                    self.getFBUserData()
                }
            }
        }
    }
    
    func getFBUserData(){
        
        if((FBSDKAccessToken.current()) != nil){
            
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    self.dict = result as! [String : AnyObject]
                    print(result!)
                    print(self.dict)
                    
                    let id = self.dict["id"]
                     let name = self.dict["name"]
                     let email = self.dict["email"]
                    
                    print(id as Any)
                    print(name as Any)
                    print(email as Any)
                    
                    
                    
                    UserDefaults.standard.set(name, forKey: "name")
                    UserDefaults.standard.synchronize()
                    
                    let TabBarController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarCon") as! UITabBarController
                    
                    
                    self.navigationController?.pushViewController(TabBarController, animated: true)
                    
                }
            })
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        //if any error stop and print the error
        if error != nil{
            
            print(error ?? "google error")
            
            return
            
        }
        
        GIDSignIn.sharedInstance().signOut()
        
        print(user.profile.email)
        
        let userId = user.userID  //103736266884310991003
        let idToken = user.authentication.idToken
        let fullName = user.profile.name
        let givenName = user.profile.givenName
        let familyName = user.profile.familyName
        let email = user.profile.email
        
        print(userId as Any)
        print(idToken as Any)
        print(fullName as Any)
        print(givenName as Any)
        print(email as Any)
        print(familyName as Any)
        
        
        UserDefaults.standard.set(fullName, forKey: "name")
        UserDefaults.standard.synchronize()
        
        
      
        
        let TabBarController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarCon") as! UITabBarController
        
        self.navigationController?.pushViewController(TabBarController, animated: true)
        
        
    }
    
    func Validate() -> Bool {
        
        if (txt_email.text?.isEmpty)!{
            
            
            
                    Alertview.isHidden = false
                    Alertview.layer.borderColor = UIColor(red:21/255, green:181/255, blue:188/255, alpha: 1).cgColor
            
                    Alertview.layer.cornerRadius = 20
                    Alertview.layer.borderWidth = 3
            
                lbl_lbl_titleAlertview2.text = UserNameValidation
                lbl_titleAlertview1.text = "Username?"
            
            return false
            
        }
        
        if(txt_password.text?.isEmpty)!{
            
            
                    Alertview.isHidden = false
                    Alertview.layer.borderColor = UIColor(red:21/255, green:181/255, blue:188/255, alpha: 1).cgColor
            
                    Alertview.layer.cornerRadius = 20
                    Alertview.layer.borderWidth = 3
            
            lbl_lbl_titleAlertview2.text = PasswordValidation
            lbl_titleAlertview1.text = "Password?"
            
            return false
            
        }
        
        if txt_password.text! == "123" && txt_email.text! == "hitesh@gmail.com" {
            
            
            Alertview.isHidden = false
            Alertview.layer.borderColor = UIColor(red:21/255, green:181/255, blue:188/255, alpha: 1).cgColor
            
            Alertview.layer.cornerRadius = 20
            Alertview.layer.borderWidth = 3
            
            lbl_lbl_titleAlertview2.text = "Your Email/Password is incorrect"
            lbl_titleAlertview1.text = "Please try again"
            
            return false
            
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        txt_email.textColor = UIColor(red:21/255, green:181/255, blue:188/255, alpha: 1)
         txt_password.textColor = UIColor(red:21/255, green:181/255, blue:188/255, alpha: 1)
        
    }
  
    @IBAction func loginwithgooglesigninClicked(_ sender: UIButton) {
        
        
        GIDSignIn.sharedInstance().delegate=self
        GIDSignIn.sharedInstance().uiDelegate=self
        GIDSignIn.sharedInstance().signIn()
        
    }
    @IBAction func Signupclicked(_ sender: UIButton) {
      
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SIGNUP_VC")
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
}

