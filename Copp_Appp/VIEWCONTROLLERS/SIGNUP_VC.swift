//
//  SIGNUP_VC.swift
//  Copp_Appp
//
//  Created by siya enterprise on 11/06/18.
//  Copyright © 2018 siya enterprise. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import FBSDKShareKit
import GoogleSignIn
import Alamofire

class SIGNUP_VC: UIViewController , GIDSignInDelegate, GIDSignInUIDelegate, UITextFieldDelegate {
    
     var dict : [String : AnyObject]!

    @IBOutlet weak var txt_confirm_pass: UITextField!
    @IBOutlet weak var txt_password: UITextField!
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_fullname: UITextField!
    @IBOutlet weak var confirm_pass_view: UIView!
    @IBOutlet weak var password_view: UIView!
    @IBOutlet weak var email_view: UIView!
    @IBOutlet weak var fullname_view: UIView!
    
    @IBOutlet weak var View_Alert: UIView!
    
    @IBOutlet weak var lbl_signup_title_alert1:UILabel!
     @IBOutlet weak var lbl_signup_title_alert2:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        txt_confirm_pass.delegate = self
        txt_fullname.delegate = self
        txt_email.delegate = self
        txt_password.delegate = self
        
        View_Alert.isHidden = true
        
        
        email_view.layer.borderColor = UIColor(red:187/255, green:225/255, blue:250/255, alpha: 1).cgColor
        
        email_view.layer.cornerRadius = 5
        email_view.layer.borderWidth = 2
        
        
        fullname_view.layer.borderColor = UIColor(red:187/255, green:225/255, blue:250/255, alpha: 1).cgColor
        
        fullname_view.layer.cornerRadius = 5
        fullname_view.layer.borderWidth = 2
        
        
        password_view.layer.borderColor = UIColor(red:187/255, green:225/255, blue:250/255, alpha: 1).cgColor
        
        password_view.layer.cornerRadius = 5
        password_view.layer.borderWidth = 2
        
        
        confirm_pass_view.layer.borderColor = UIColor(red:187/255, green:225/255, blue:250/255, alpha: 1).cgColor
        
        confirm_pass_view.layer.cornerRadius = 5
        confirm_pass_view.layer.borderWidth = 2


       
    }
    
    
    @IBAction func Signupbtn(_ sender: UIButton) {
        
        
        self.view.endEditing(true)
        if Validate(){
            
            
            let TabBarController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarCon") as! UITabBarController
            
            self.navigationController?.pushViewController(TabBarController, animated: true)
            
        }
        
    }
    
    func Validate() -> Bool {
        
        if(txt_fullname.text?.isEmpty)!{
            
            
            View_Alert.isHidden = false
            View_Alert.layer.borderColor = UIColor(red:21/255, green:181/255, blue:188/255, alpha: 1).cgColor
            
            View_Alert.layer.cornerRadius = 20
            View_Alert.layer.borderWidth = 3
            
            lbl_signup_title_alert2.text = "Please enter fullname"
            lbl_signup_title_alert1.text = " Fullname?"
            
            return false
            
        }
        
        if (txt_email.text?.isEmpty)!{
            
            
            
            View_Alert.isHidden = false
            View_Alert.layer.borderColor = UIColor(red:21/255, green:181/255, blue:188/255, alpha: 1).cgColor
            
            View_Alert.layer.cornerRadius = 20
            View_Alert.layer.borderWidth = 3
            
            lbl_signup_title_alert2.text = "Please enter Email"
            lbl_signup_title_alert1.text = "Email?"
            
            return false
            
        }
        
        if(txt_password.text?.isEmpty)!{
            
            
            View_Alert.isHidden = false
            View_Alert.layer.borderColor = UIColor(red:21/255, green:181/255, blue:188/255, alpha: 1).cgColor
            
            View_Alert.layer.cornerRadius = 20
            View_Alert.layer.borderWidth = 3
            
            lbl_signup_title_alert2.text = "Please enter Password"
            lbl_signup_title_alert1.text = "Password?"
            
            return false
            
        }
        
        if(txt_confirm_pass.text?.isEmpty)!{
            
            
            View_Alert.isHidden = false
            View_Alert.layer.borderColor = UIColor(red:21/255, green:181/255, blue:188/255, alpha: 1).cgColor
            
            View_Alert.layer.cornerRadius = 20
            View_Alert.layer.borderWidth = 3
            
            lbl_signup_title_alert2.text = "Please enter Repeat Password"
            lbl_signup_title_alert1.text = " Repeat Password?"
            
            return false
            
        }
    
        return true
        
    }
    
    @IBAction func OkbtnClicked(_ sender: UIButton) {
        
        
        View_Alert.isHidden = true
        
    }

    @IBAction func BackbtnClicked(_ sender: UIButton) {
        
       self.navigationController?.popViewController(animated: true)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
   
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        txt_email.textColor = UIColor(red:21/255, green:181/255, blue:188/255, alpha: 1)
        txt_password.textColor = UIColor(red:21/255, green:181/255, blue:188/255, alpha: 1)
         txt_fullname.textColor = UIColor(red:21/255, green:181/255, blue:188/255, alpha: 1)
         txt_confirm_pass.textColor = UIColor(red:21/255, green:181/255, blue:188/255, alpha: 1)
        
    }
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        //if any error stop and print the error
        if error != nil{
            print(error ?? "google error")
            return
        }
        GIDSignIn.sharedInstance().signOut()
        
        print(user.profile.email)
        
        let userId = user.userID  //103736266884310991003
        let idToken = user.authentication.idToken
        let fullName = user.profile.name
        let givenName = user.profile.givenName
        let familyName = user.profile.familyName
        let email = user.profile.email
        
        print(userId as Any)
        print(idToken as Any)
        print(fullName as Any)
        print(givenName as Any)
        print(email as Any)
        print(familyName as Any)
        
        
        UserDefaults.standard.set(fullName, forKey: "name")
        UserDefaults.standard.synchronize()
        
        let TabBarController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarCon") as! UITabBarController
        
        self.navigationController?.pushViewController(TabBarController, animated: true)
    
    }
    @IBAction func loginwithgooglesigninClicked(_ sender: UIButton) {
        
        
        GIDSignIn.sharedInstance().delegate=self
        GIDSignIn.sharedInstance().uiDelegate=self
        GIDSignIn.sharedInstance().signIn()
        
    }
    
    @IBAction func SignupwithfacebookClicked(_ sender: UIButton) {
        
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) -> Void in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                
                if (result?.isCancelled)!{
                    return
                }
                if(fbloginresult.grantedPermissions.contains("email"))
                {
                    self.getFBUserData()
                }
            }
        }
        
        
    }
    
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    self.dict = result as! [String : AnyObject]
                    print(result!)
                    print(self.dict)
                    
                    let id = self.dict["id"]
                    let name = self.dict["name"]
                    let email = self.dict["email"]
                    
                    print(id as Any)
                    print(name as Any)
                    print(email as Any)
                    UserDefaults.standard.set(name, forKey: "name")
                    UserDefaults.standard.synchronize()
                    
                    let TabBarController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarCon") as! UITabBarController
                    
                    self.navigationController?.pushViewController(TabBarController, animated: true)
                    
                    
                    
                }
            })
        }
    }
    

}
