//
//  EditProfileVC.swift
//  Copp_Appp
//
//  Created by siya enterprise on 13/06/18.
//  Copyright © 2018 siya enterprise. All rights reserved.
//

import UIKit

class EditProfileVC: UIViewController {

    @IBOutlet weak var txt_firstname: UITextField!
    @IBOutlet weak var txt_lastname: UITextField!
    @IBOutlet weak var txt_Address1: UITextField!
    @IBOutlet weak var txt_Address2: UITextField!
    @IBOutlet weak var txt_city: UITextField!
    @IBOutlet weak var txt_postalcode: UITextField!
    @IBOutlet weak var txt_country: UITextField!
    @IBOutlet weak var txt_phone: UITextField!
    
     @IBOutlet weak var FirstnameView: UIView!
     @IBOutlet weak var lastnameView: UIView!
     @IBOutlet weak var address1View: UIView!
     @IBOutlet weak var address2View: UIView!
     @IBOutlet weak var cityView: UIView!
     @IBOutlet weak var postalcodeView: UIView!
     @IBOutlet weak var countryView: UIView!
     @IBOutlet weak var phoneView: UIView!
    
    
    @IBOutlet weak var lbl_signup_title_alert1:UILabel!
    @IBOutlet weak var lbl_signup_title_alert2:UILabel!
    @IBOutlet weak var View_Alert: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         View_Alert.isHidden = true
        
          tabBarController?.tabBar.isHidden = true
        
        FirstnameView.layer.borderColor = UIColor(red:187/255, green:225/255, blue:250/255, alpha: 1).cgColor
        
        FirstnameView.layer.cornerRadius = 5
        FirstnameView.layer.borderWidth = 2
        
        
        lastnameView.layer.borderColor = UIColor(red:187/255, green:225/255, blue:250/255, alpha: 1).cgColor
        
        lastnameView.layer.cornerRadius = 5
        lastnameView.layer.borderWidth = 2
        
        address1View.layer.borderColor = UIColor(red:187/255, green:225/255, blue:250/255, alpha: 1).cgColor
        
        address1View.layer.cornerRadius = 5
        address1View.layer.borderWidth = 2
        
        
        address2View.layer.borderColor = UIColor(red:187/255, green:225/255, blue:250/255, alpha: 1).cgColor
        
        address2View.layer.cornerRadius = 5
        address2View.layer.borderWidth = 2
        
        
        cityView.layer.borderColor = UIColor(red:187/255, green:225/255, blue:250/255, alpha: 1).cgColor
        
        cityView.layer.cornerRadius = 5
        cityView.layer.borderWidth = 2
        
        
        postalcodeView.layer.borderColor = UIColor(red:187/255, green:225/255, blue:250/255, alpha: 1).cgColor
        
        postalcodeView.layer.cornerRadius = 5
        postalcodeView.layer.borderWidth = 2
        
        
        countryView.layer.borderColor = UIColor(red:187/255, green:225/255, blue:250/255, alpha: 1).cgColor
        
        countryView.layer.cornerRadius = 5
        countryView.layer.borderWidth = 2
        
        
        phoneView.layer.borderColor = UIColor(red:187/255, green:225/255, blue:250/255, alpha: 1).cgColor
        
        phoneView.layer.cornerRadius = 5
        phoneView.layer.borderWidth = 2

        
    }
    
    @IBAction func Savebtn(_ sender: UIButton) {
        
        
        self.view.endEditing(true)
        if Validate(){
            
            
            let TabBarController = self.storyboard?.instantiateViewController(withIdentifier: "TabBarCon") as! UITabBarController
            
            self.navigationController?.pushViewController(TabBarController, animated: true)
            
            
        }
        
    }
    
    
    func Validate() -> Bool {
        
        
        
        if(txt_firstname.text?.isEmpty)!{
            
            
            View_Alert.isHidden = false
            View_Alert.layer.borderColor = UIColor(red:21/255, green:181/255, blue:188/255, alpha: 1).cgColor
            
            View_Alert.layer.cornerRadius = 20
            View_Alert.layer.borderWidth = 3
            
            lbl_signup_title_alert2.text = "Please enter firstname"
            lbl_signup_title_alert1.text = " firstname?"
            
            return false
            
        }
        
        if (txt_lastname.text?.isEmpty)!{
            
            
            
            View_Alert.isHidden = false
            View_Alert.layer.borderColor = UIColor(red:21/255, green:181/255, blue:188/255, alpha: 1).cgColor
            
            View_Alert.layer.cornerRadius = 20
            View_Alert.layer.borderWidth = 3
            
            lbl_signup_title_alert2.text = "Please enter lastname"
            lbl_signup_title_alert1.text = "lastname?"
            
            return false
            
        }
        
        if(txt_Address1.text?.isEmpty)!{
            
            
            View_Alert.isHidden = false
            View_Alert.layer.borderColor = UIColor(red:21/255, green:181/255, blue:188/255, alpha: 1).cgColor
            
            View_Alert.layer.cornerRadius = 20
            View_Alert.layer.borderWidth = 3
            
            lbl_signup_title_alert2.text = "Please enter Address1"
            lbl_signup_title_alert1.text = "Address1?"
            
            return false
            
        }
        
        
        if(txt_Address2.text?.isEmpty)!{
            
            
            View_Alert.isHidden = false
            View_Alert.layer.borderColor = UIColor(red:21/255, green:181/255, blue:188/255, alpha: 1).cgColor
            
            View_Alert.layer.cornerRadius = 20
            View_Alert.layer.borderWidth = 3
            
            lbl_signup_title_alert2.text = "Please enter Address2"
            lbl_signup_title_alert1.text = "Address2?"
            
            return false
            
        }
        
        
        if(txt_city.text?.isEmpty)!{
            
            
            View_Alert.isHidden = false
            View_Alert.layer.borderColor = UIColor(red:21/255, green:181/255, blue:188/255, alpha: 1).cgColor
            
            View_Alert.layer.cornerRadius = 20
            View_Alert.layer.borderWidth = 3
            
            lbl_signup_title_alert2.text = "Please enter City"
            lbl_signup_title_alert1.text = "City?"
            
            return false
            
        }
        
        if(txt_postalcode.text?.isEmpty)!{
            
            
            View_Alert.isHidden = false
            View_Alert.layer.borderColor = UIColor(red:21/255, green:181/255, blue:188/255, alpha: 1).cgColor
            
            View_Alert.layer.cornerRadius = 20
            View_Alert.layer.borderWidth = 3
            
            lbl_signup_title_alert2.text = "Please enter PostalCode"
            lbl_signup_title_alert1.text = "PostalCode?"
            
            return false
            
        }
        
        if(txt_country.text?.isEmpty)!{
            
            
            View_Alert.isHidden = false
            View_Alert.layer.borderColor = UIColor(red:21/255, green:181/255, blue:188/255, alpha: 1).cgColor
            
            View_Alert.layer.cornerRadius = 20
            View_Alert.layer.borderWidth = 3
            
            lbl_signup_title_alert2.text = "Please enter Country"
            lbl_signup_title_alert1.text = "Country?"
            
            return false
            
        }
        
        if(txt_phone.text?.isEmpty)!{
            
            
            View_Alert.isHidden = false
            View_Alert.layer.borderColor = UIColor(red:21/255, green:181/255, blue:188/255, alpha: 1).cgColor
            
            View_Alert.layer.cornerRadius = 20
            View_Alert.layer.borderWidth = 3
            
            lbl_signup_title_alert2.text = "Please enter Phone number"
            lbl_signup_title_alert1.text = "Phone?"
            
            return false
            
        }
    
        return true
    }
    @IBAction func OkbtnClicked(_ sender: UIButton) {
        
        
        View_Alert.isHidden = true
        
    }
    @IBAction func BackbtnClicked(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
}
