//
//  DroplistMembershipVC.swift
//  Copp_Appp
//
//  Created by siya enterprise on 12/06/18.
//  Copyright © 2018 siya enterprise. All rights reserved.
//

import UIKit

class DroplistMembershipVC: UIViewController {
    
    @IBOutlet weak var radioButton11: RadioButton!
    @IBOutlet weak var radioButton22: RadioButton!
    @IBOutlet weak var radioButton33: RadioButton!
    
    lazy var radioButtons: [RadioButton] = {
        return [
            self.radioButton11,
            self.radioButton22,
            self.radioButton33
            ]
    }()

    @IBOutlet weak var viewmembership1: UIView!
     @IBOutlet weak var viewmembership2: UIView!
     @IBOutlet weak var viewmembership3: UIView!
    
    @IBOutlet weak var mainSubview: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        mainSubview.dropShadow(color: UIColor(red:0/255, green:0/255, blue:0/255, alpha: 1), opacity: 0.3, offSet: CGSize(width: -1, height: 1), radius: 3, scale: true)
        
        
        self.viewmembership1.layer.cornerRadius = self.viewmembership1.frame.size.width / 2
        
        
        self.viewmembership1.clipsToBounds = true
        
        
        self.viewmembership2.layer.cornerRadius = self.viewmembership2.frame.size.width / 2
        
        
        self.viewmembership2.clipsToBounds = true
        
        
        self.viewmembership3.layer.cornerRadius = self.viewmembership3.frame.size.width / 2
        
        
        self.viewmembership3.clipsToBounds = true
        
//        mainSubview.layer.borderColor = UIColor(red:112/255, green:112/255, blue:112/255, alpha: 1).cgColor
//        mainSubview.layer.borderWidth = 1.0
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    @IBAction func onRadioButtonClicked(_ sender: RadioButton) {
        updateRadioButton(sender)
    }
    @IBAction func onSecondRadioButtonClicked(_ sender: RadioButton) {
        updateRadioButton(sender)
    }
    
    @IBAction func onThirdRadioButtonClicked(_ sender: RadioButton) {
        updateRadioButton(sender)
    }
    
    func updateRadioButton(_ sender: RadioButton){
        
        radioButtons.forEach { $0.isSelected = false }
        sender.isSelected = !sender.isSelected
        
    }
    
    func getSelectedRadioButton() -> RadioButton? {
        var radioButton: RadioButton?
        radioButtons.forEach { if($0.isSelected){ radioButton =  $0 } }
        return radioButton
    }
    
    @IBAction func yearlyplanclickevent(_ sender: UIButton) {
        
        
        
    }
    @IBAction func monthlyplanclickevent(_ sender: UIButton) {
         // viewmembership.isHidden = false
        
    }
    
    @IBAction func backbutton(_ sender: UIButton) {
        
     self.navigationController?.popViewController(animated: true)
       
    }
    @IBAction func CancelbtnClicked(_ sender: UIButton) {
        
       // viewmembership.isHidden = true
        
    }
    @IBAction func Gotopaymentscreen(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PaymentVC")
        
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
}
extension UIView {
    
    // OUTPUT 1
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: -1, height: 1)
        layer.shadowRadius = 1
        
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    // OUTPUT 2
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}
