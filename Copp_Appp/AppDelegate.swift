//
//  AppDelegate.swift
//  Copp_Appp
//
//  Created by siya enterprise on 11/06/18.
//  Copyright © 2018 siya enterprise. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import GoogleSignIn 

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        
      //  private static final String SHOP_DOMAIN = "premecopp.myshopify.com";
       // private static final String API_KEY = "969d5cf8ee16f877cc5164b1304f2251";
        
       
        GIDSignIn.sharedInstance().clientID = "551569441225-2stq24t5qqa1483aj5pc53l35cltkk4g.apps.googleusercontent.com"
        
      
         return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
        FBSDKAppEvents.activateApp()
       
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        
        GIDSignIn.sharedInstance().handle(url, sourceApplication: sourceApplication, annotation: annotation)
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        

       
        
        
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
       
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        
        
    }


}

